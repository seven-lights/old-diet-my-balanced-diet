/**
 * Created by Еввгений on 13.07.14.
 */
function tooggleSideMenu() {
    $('.sidebar .side-name').unbind('click').click(function(){
        $(this).unbind('click');
        $(this).toggleClass('active');
        $('#' + $(this).attr('for')).slideToggle('medium', function(){
            $(this).toggleClass('active');
            tooggleSideMenu();
        });
    });
}
$(function(){
    tooggleSideMenu();
});