<?php /* @var $this Controller */
Yii::app()->clientScript->registerCssFile('css/main.css');
Yii::app()->clientScript->registerCssFile('css/fonts.css');
//Yii::app()->clientScript->registerPackage('main');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div id="hdr-wrap">
    <div id="top-nav">
        <div id="logo">
            <a href="/">Мое сбалансированное питание</a>
            <div>Всё о питании и здоровье</div>
        </div>
        <div class="nav" id="menu-bar">
            <ul class="nice-menu">
                <li class="menuparent">
                    <a title="" href="/fitness">Fitness</a>
                </li>
                <li class="menuparent">
                    <a title="" href="/healthy-eating">Healthy Eating</a>
                </li>
                <li class="menuparent">
                    <a title="" href="/weight-loss">Weight Loss</a>
                </li>
                <li class="menuparent">
                    <a title="" href="/lifestyle">Lifestyle</a>
                </li>
                <li class="menuparent">
                    <a title="" href="/celebrities">Celebrities</a>
                </li>
            </ul>
        </div>
        <?= /*'<div id="top-nav-btns">
            <input type="text" name="textquery" value="" class="search-btn" id="txt-search">
        </div>'*/'' ?>
    </div>
</div>
<div class="container">
    <div class="layout">

        <?php /*<div id="header">
            <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
        </div><!-- header -->

        <div id="mainmenu">
            <?php $this->widget('zii.widgets.CMenu',array(
                'items'=>array(
                    array('label'=>'Home', 'url'=>array('/site/index')),
                    array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                    array('label'=>'Contact', 'url'=>array('/site/contact')),
                    array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                ),
            )); ?>
        </div><!-- mainmenu -->*/ ?>
        <?php if(isset($this->breadcrumbs)):?>
            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
            )); ?><!-- breadcrumbs -->
        <?php endif?>

        <?php echo $content; ?>
    </div><!-- page -->
</div>
<div class="nav"></div>
<div class="footer nav">
    <div id="wrap-footer-menu">
        <div id="footer-menu">
            <div class="footer-column">
                <img src="/i/sl-logo.png" alt="Семь огней / Seven lights" width="150">
            </div>
            <div class="footer-column">
                <ul>
                    <li>Главная</li>
                    <li>Все коктейли</li>
                    <li>Коллекции</li>
                    <li>Бокалы</li>
                    <li>Компоненты</li>
                    <li>Фичи</li>
                </ul>
            </div>
            <div class="footer-column">
                <ul>
                    <li>Главная</li>
                    <li>Все коктейли</li>
                    <li>Коллекции</li>
                    <li>Бокалы</li>
                    <li>Компоненты</li>
                    <li>Фичи</li>
                </ul>
            </div>
            <div class="footer-column">
                <ul>
                    <li>Главная</li>
                    <li>Все коктейли</li>
                    <li>Коллекции</li>
                    <li>Бокалы</li>
                    <li>Компоненты</li>
                    <li>Фичи</li>
                </ul>
            </div>
            <div class="footer-column">
                <ul>
                    <li><img src="/i/seventeen-plus.png" alt="18+"></li>
                    <li class="small">Материалы сайта предназначены для аудитории старше 18 лет</li>
                    <li class="small">Информация на сайте представлена исключительно в справочных целях</li>
                    <li class="small">Обязательно проконсультируйтесь с врачом</li>
                </ul>
            </div>
        </div>
    </div>
    <div id="wrap-copyright" class="nav">
                <span id="copyright">
                    &copy; 2014<?= date('Y') > 2014 ?: '' ?> Семь огней / Seven lights<br/>
                    &copy; 2014<?= date('Y') > 2014 ?: '' ?> Русанов Евгений<br/>
                </span>
    </div>
</div><!-- footer -->
</body>
</html>
